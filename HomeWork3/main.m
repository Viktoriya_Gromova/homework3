//
//  main.m
//  HomeWork3
//
//  Created by Victoria on 15.11.15.
//  Copyright © 2015 ViktoriyaGromova. All rights reserved.
//

#import <Foundation/Foundation.h>

double divideTwoNumbers();

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        double firstValue = 72.99;
        int secondValue = 8;
        NSLog(@"\nThe division of two value equals %f", divideTwoNumbers(firstValue, secondValue));
    }
    return 0;
}

double divideTwoNumbers(double num1, int num2)
{
    return num1/num2;
}